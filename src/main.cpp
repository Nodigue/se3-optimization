#include "ceres_optimization_test/TransformOperations.h"
#include "ceres_optimization_test/TransformOptimization.h"

#include <Eigen/Core>

#include <vector>
#include <iostream>

int main(int, char **)
{
    Eigen::Matrix4d T_target = Eigen::Matrix4d::Identity();
    T_target.block<3, 1>(0, 3) = Eigen::Vector3d(0.1, 0.2, 0.3);
    T_target.block<3, 3>(0, 0) = Eigen::AngleAxisd(0.4, Eigen::Vector3d(0.5, 0.6, 0.7).normalized()).toRotationMatrix();

    Eigen::Matrix4d T_init = Eigen::Matrix4d::Identity();
    std::vector<Eigen::Matrix4d> TmList = {T_target};

    auto solver = SE3::optimization::Solver(T_init, TmList);
    solver.solve();

    std::cout << T_target << "\n\n";
    std::cout << solver.getEstimate() << "\n";

    return 0;
}
