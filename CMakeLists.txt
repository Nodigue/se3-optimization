cmake_minimum_required(VERSION 3.8)
project(ceres_optimization_test)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
	add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(Ceres REQUIRED)

include_directories(include)

add_executable(main
	src/main.cpp
)

target_link_libraries(main
	Eigen3::Eigen
	Ceres::ceres
)

install(
	TARGETS
    	main
	DESTINATION
    	lib/${PROJECT_NAME}
)

ament_package()