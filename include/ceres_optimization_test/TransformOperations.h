#pragma once

#include "RotationOperations.h"

#include <Eigen/Core>
#include <stdexcept>

namespace SE3
{
     /**
     * @brief Check if a 4x4 matrix is in \f$\text{SE}(3)\f$ (is a homogeneous transformation matrix).
     * We define \f$\text{SE}(3)\f$ as :
     * \f[
     * SE(3) = \left\{ T = \begin{bmatrix} R & p \\ \mathbf{0} & 1 \end{bmatrix} \Big|\ R \in SO(3),\ p \in \mathbb{R}^3 \right\} \subset \mathbb{R}^{4\times4}
     * \f]
     *
     * @param[in] p_mat Input 4x4 matrix.
     *
     * @return true, if the matrix is in \f$\text{SE}(3)\f$.
     * @return false, if the matrix is NOT in \f$\text{SE}(3)\f$.
     */
    template <typename T>
    bool isInSE3(const Eigen::Matrix<T, 4, 4>& p_mat)
    {
        // Check if 3x3 left upper part is a SO(3) matrix.
        const bool hasSO3Matrix = SO3::isInSO3<T>(p_mat.template block <3, 3>(0, 0));

        // Check if last line is [0, 0, 0, 1]
        Eigen::Matrix<T, 4, 1> lastLine(T(0), T(0), T(0), T(1));
        const bool hasLastLine = p_mat.template block <1, 4>(3, 0).isApprox(lastLine.transpose());

        return hasSO3Matrix && hasLastLine;
    }

    /**
     * @brief Isomorphism to pass from the Lie algebra \f$\mathfrak{se}(3)\f$ to the associated cartesian vector space \f$\mathbb{R]^{6}\f$.
     * The element of the cartesian vector space is sometimes denoted as a twist vector or the exponential coordinates
     * of the transformation.
     *
     * See hat for the inverse operation.
     *
     * @param[in] p_V_x Element of \f$\mathfrak{se}(3)\f$.
     * @return Eigen::Matrix<T, 6, 1> Twist vector.
     */
    template <typename T>
    Eigen::Matrix<T, 6, 1> vee(const Eigen::Matrix<T, 4, 4>& p_V_x)
    {
        Eigen::Matrix<T, 6, 1> V = Eigen::Matrix<T, 6, 1>::Zero();

        V(0) = p_V_x(0, 3);
        V(1) = p_V_x(1, 3);
        V(2) = p_V_x(2, 3);
        V(3) = p_V_x(2, 1);
        V(4) = p_V_x(0, 2);
        V(5) = p_V_x(1, 0);

        return V;
    }

    /**
     * @brief Isomorphism to pass from the cartesian vector space \f$\mathbb{R]^{6}\f$ to the Lie algebra \f$\mathfrak{se}(3)\f$.
     * The element of the cartesian vector space is sometimes denoted as a twist vector or the exponential
	 * coordinates of the transformation.
     *
     * See hat for the inverse operation.
     *
     * @param[in] p_V Twist vector.
     * @return Eigen::Matrix<T, 4, 4> Corresponding element of \f$\mathfrak{se}(3)\f$.
     */
    template <typename T>
    Eigen::Matrix<T, 4, 4> hat(const Eigen::Matrix<T, 6, 1>& p_V)
    {
        Eigen::Matrix<T, 4, 4> V_x = Eigen::Matrix<T, 4, 4>::Zero();

        V_x.template block<3, 3>(0, 0) = SO3::hat<T>(p_V.tail(3));
        V_x.template block<3, 1>(0, 3) = p_V.head(3);

        return V_x;
    }

    /**
     * @brief Normalize a 6x1 twist vector V and return its norm.
     * A unit twist (normalized twist or screw) is a twist where:
     *  - the rotation part has unit magnitude.
     *  - if the rotational part is zero, then the translational part has unit magnitude.
     *
	 * @param[in] p_V Twist vector that should be normalized.
	 * @param[out] p_out_theta Norm of the twist vector.
	 * @param[out] p_out_S Normalized twist vector (Also called screw axis).
     */
    template <typename T>
    void normalizeTwistVector(const Eigen::Matrix<T, 6, 1>& p_V, T& p_out_theta, Eigen::Matrix<T, 6, 1>& p_out_S)
    {
        T theta = T(0);
        T wNorm = p_V.tail(3).norm();

        if (wNorm > T(0))
        {
            theta = wNorm;
        }
        else
        {
            theta = p_V.head(3).norm();
        }

        p_out_theta = theta;
        p_out_S = p_V / theta;
    }

    /**
	 * @brief Convenient exponential map to pass from the cartesian vector space \f$\mathbb{R}^{6}\f$ to the associated Lie group \f$\text{SE}(3)\f$.
	 * More generically speaking, it converts a twist vector to the associated transformation matrix.
     *
	 * @param[in] p_V Twist vector.
	 * @return Eigen::Matrix<T, 4, 4> Associated transformation matrix.
	 */
    template <typename T>
    Eigen::Matrix<T, 4, 4> Exp(const Eigen::Matrix<T, 6, 1>& p_V)
    {
        if (p_V.isApprox(Eigen::Matrix<T, 6, 1>::Zero()))
        {
            return Eigen::Matrix<T, 4, 4>::Identity();
        }

        Eigen::Matrix<T, 6, 1> S = Eigen::Matrix<T, 6, 1>::Zero();
        T theta = T(0);
        normalizeTwistVector<T>(p_V, theta, S);

        Eigen::Matrix<T, 3, 1> v = S.head(3);

        Eigen::Matrix<T, 3, 1> w = S.tail(3);
        Eigen::Matrix<T, 3, 3> w_x = SO3::hat<T>(w);

        Eigen::Matrix<T, 4, 4> Tm = Eigen::Matrix<T, 4, 4>::Identity();

        Eigen::Matrix<T, 3, 3> G = Eigen::Matrix<T, 3, 3>::Identity() * theta + (T(1) - cos(theta)) * w_x + (theta - sin(theta)) * (w_x * w_x);
        Tm.template block <3, 3>(0, 0) = SO3::Exp<T>(w * theta);
        Tm.template block <3, 1>(0, 3) = G * v;

        return Tm;
    }

    /**
	 * @brief Convenient logarithm map to pass from the Lie group \f$\text{SE}(3)\f$ to the associated cartesian vector space \f$\mathbb{R}^{6}\f$.
	 * More generically speaking, it converts a transformation matrix to the associated twist vector.
	 *
	 * @param[in] p_R Transformation matrix.
	 * @param[in] p_checkValidity Should we check that the input matrix is valid or not ?
	 * @return Eigen::Matrix<T, 3, 1> Associated twist vector.
	 */
    template<typename T>
    Eigen::Matrix<T, 6, 1> Log(const Eigen::Matrix<T, 4, 4>& p_T, const bool p_checkValidity = true)
    {
        if (p_checkValidity && !isInSE3<T>(p_T))
        {
            std::cout << p_T << "\n";
            throw std::runtime_error("Input matrix is not in SE(3)");
        }

        Eigen::Matrix<T, 3, 1> p = p_T.template block<3, 1>(0, 3);
        Eigen::Matrix<T, 3, 3> R = p_T.template block<3, 3>(0, 0);

        if (R.isApprox(Eigen::Matrix<T, 3, 3>::Identity()))
        {
            Eigen::Matrix<T, 4, 4> V_x  = Eigen::Matrix<T, 4, 4>::Zero();
            V_x.template block <3, 3>(0, 0) = Eigen::Matrix<T, 3, 3>::Zero();
            V_x.template block <3, 1>(0, 3) = p;

            return vee<T>(V_x);
        }
        else
        {
            Eigen::Matrix<T, 3, 1> w = SO3::Log<T>(R, p_checkValidity);
            Eigen::Matrix<T, 3, 3> w_x = SO3::hat<T>(w);

            T theta = w.norm();

            Eigen::Matrix<T, 3, 3> GInv = Eigen::Matrix<T, 3, 3>::Identity() - (w_x / T(2)) + (T(1) / theta) * ((T(1) / theta) - (T(1)  / (T(2) * tan(theta / T(2))))) * (w_x * w_x);
            Eigen::Matrix<T, 3, 1> v = GInv * p;

            Eigen::Matrix<T, 6, 1> V = Eigen::Matrix<T, 6, 1>::Zero();
            V.head(3) = v;
            V.tail(3) = w;

            return V;
        }
    }

    /**
     * @brief Compute the adjoint representation of a \f$\text{SE}(3)\f$ homogeneous transformation matrix.
     *
     * @param[in] p_T Input \f$\text{SE}(3)\f$ transformation matrix.
     * @return Eigen::Matrix<T, 6, 6> Adjoint representation.
     */
    template <typename T>
    Eigen::Matrix<T, 6, 6> adjoint(const Eigen::Matrix<T, 4, 4>& p_T)
    {
        const Eigen::Matrix<T, 3, 1> p = p_T.template block<3, 1>(0, 3);
        const Eigen::Matrix<T, 3, 3> R = p_T.template block<3, 3>(0, 0);

        Eigen::Matrix<T, 6, 6> adj = Eigen::Matrix<T, 6, 6>::Zero();
        adj.template block<3, 3>(0, 0) = R;
        adj.template block<3, 3>(0, 3) = SO3::hat<T>(p) * R;
        adj.template block<3, 3>(3, 0) = Eigen::Matrix<T, 3, 3>::Zero();
        adj.template block<3, 3>(3, 3) = R;

        return adj;
    }

    /**
     * @brief Implement the Right-Plus \f$\boxplus\f$ operator. This compute the result of moving along p_V in the
     * tangent vector space and projecting back into the manifold.
     * This operation is also called retraction.
     *
     * In this case, the operation is applying from the right, the increment p_V should be expressed in the local frame.
     *
     * @param[in] p_T Element of \f$\text{SE}(3)\f$.
     * @param[in] p_V Increment in \f$\mathbb{R}^{6}\f$.
     * @return Eigen::Matrix<T, 4, 4> Incremented element expressed in \f$\text{SE}(3)\f$.
     */
    template <typename T>
    Eigen::Matrix<T, 4, 4> rightPlus(const Eigen::Matrix<T, 4, 4>& p_T, const Eigen::Matrix<T, 6, 1>& p_V)
    {
        return p_T * Exp<T>(p_V);
    }

    /**
     * @brief Implement the Left-Plus \f$\boxplus\f$ operator. This compute the result of moving along p_V in the
     * tangent vector space and projecting back into the manifold.
     * This operation is also called retraction.
     *
     * In this case, the operation is applying from the left, the increment p_V should be expressed in the global frame.
     *
     * @param[in] p_V Increment in \f$\mathbb{R}^{6}\f$.
     * @param[in] p_T Element of \f$\text{SE}(3)\f$.
     * @return Eigen::Matrix<T, 4, 4> Incremented element expressed in \f$\text{SE}(3)\f$.
     */
    template <typename T>
    Eigen::Matrix<T, 4, 4> leftPlus(const Eigen::Matrix<T, 6, 1>& p_V, const Eigen::Matrix<T, 4, 4>& p_T)
    {
        return Exp<T>(p_V) * p_T;
    }

    /**
     * @brief Implement the Right-Minus \f$\boxminus\f$ operator. Given two element p_T1 and p_T2 on the manifold,
     * compute the change to p_T1 in the tangent vector space, that will take to p_T2.
     * This is the inverse operation of the Right-Plus operator. The resulting increment is then expressed in the local
     * frame.
     *
     * @param[in] p_T2 Second element of \f$\text{SE}(3)\f$.
     * @param[in] p_T1 First element of \f$\text{SE}(3)\f$.
     * @param[in] p_checkValidity Should we check that the input matrix is valid or not ?
     * @return Eigen::Matrix<T, 6, 1> Increment expressed in \f$\mathbb{R}^{6}\f$ that takes p_T1 to p_T2.
     */
    template <typename T>
    Eigen::Matrix<T, 6, 1> rightMinus(const Eigen::Matrix<T, 4, 4>& p_T2, const Eigen::Matrix<T, 4, 4>& p_T1, const bool p_checkValidity = true)
    {
        return Log<T>(p_T1.inverse() * p_T2, p_checkValidity);
    }

    /**
     * @brief Implement the Left-Minus \f$\boxminus\f$ operator. Given two element p_T1 and p_T2 on the manifold,
     * compute the change to p_T1 in the tangent vector space, that will take to p_T2.
     * This is the inverse operation of the Left-Plus operator. The resulting increment is then expressed in the global
     * frame.
     *
     * @param[in] p_T2 Second element of \f$\text{SE}(3)\f$.
     * @param[in] p_T1 First element of \f$\text{SE}(3)\f$.
     * @param[in] p_checkValidity Should we check that the input matrix is valid or not ?
     * @return Eigen::Matrix<T, 6, 1> Increment expressed in \f$\mathbb{R}^{6}\f$ that takes p_T1 to p_T2.
     */
    template <typename T>
    Eigen::Matrix<T, 6, 1> leftMinus(const Eigen::Matrix<T, 4, 4>& p_T2, const Eigen::Matrix<T, 4, 4>& p_T1, const bool p_checkValidity = true)
    {
        return Log<T>(p_T2 * p_T1.inverse(), p_checkValidity);
    }
}