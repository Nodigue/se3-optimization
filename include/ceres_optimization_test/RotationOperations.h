#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdexcept>
#include <iostream>

namespace SO3
{
	/**
     * @brief Check if a 3x3 matrix is in \f$\text{SO}(3)\f$ (is a rotation matrix).
	 * We define \f$\text{SO}(3)\f$ as :
	 * \f[
	 * \bm{R} \in \text{SO}(3) = \left\{\bm{R} \in \mathbb{R}^{3\times3}\ |\ \bm{R}^{T}\bm{R} = \bm{I},\ \text{det}(\bm{R}) = +1\right\}\subset\mathbb{R}^{3\times3}
	 * \f]
     *
     * @param[in] p_mat Input 3x3 matrix.
     * @return true, if the matrix is in \f$\text{SO}(3)\f$.
     * @return false, if the matrix is NOT in \f$\text{SO}(3)\f$.
     */
	template <typename T>
	bool isInSO3(const Eigen::Matrix<T, 3, 3>& p_mat)
	{
		// Check if R * R^T = R^T * R = I
		const bool orthogonality = p_mat.isUnitary();

		// Check if the determinant is +1.
		const bool determinant = abs(p_mat.determinant() - T(1)) < 1e-9;

		return orthogonality && determinant;
	}

	/**
     * @brief Isomorphism to pass from the Lie algebra \f$\mathfrak{so}(3)\f$ to the associated cartesian vector space \f$\mathbb{R]^{3}\f$.
	 * The element of the cartesian vector space is sometimes denoted as a rotation vector or the exponential coordinates
	 * of the rotation.
	 *
     * See hat for the inverse operation.
     *
     * @param[in] p_w_x Element of \f$\mathfrak{so}(3)\f$.
     * @return Eigen::Matrix<T, 3, 1> Associated rotation vector.
     */
	template <typename T>
	Eigen::Matrix<T, 3, 1> vee(const Eigen::Matrix<T, 3, 3>& p_w_x)
	{
		Eigen::Matrix<T, 3, 1> w = Eigen::Matrix<T, 3, 1>::Zero();

		w(0) = p_w_x(2, 1);
		w(1) = p_w_x(0, 2);
		w(2) = p_w_x(1, 0);

		return w;
	}

	/**
     * @brief Isomorphism to pass from the cartesian vector space \f$\mathbb{R]^{3}\f$ to the Lie algebra \f$\mathfrak{so}(3)\f$.
	 * The element of the cartesian vector space is sometimes denoted as a rotation vector or the exponential coordinates
	 * of the rotation.
     *
	 * See hat for the inverse operation.
     *
     * @param[in] p_w Rotation vector.
     * @return Eigen::Matrix<T, 3, 3> Corresponding element of \f$\mathfrak{so}(3)\f$.
     */
	template <typename T>
	Eigen::Matrix<T, 3, 3> hat(const Eigen::Matrix<T, 3, 1>& p_w)
	{
		Eigen::Matrix<T, 3, 3> w_x = Eigen::Matrix<T, 3, 3>::Zero();

		w_x(0, 1) = -p_w(2, 0);
		w_x(1, 0) =  p_w(2, 0);
		w_x(0, 2) =  p_w(1, 0);
		w_x(2, 0) = -p_w(1, 0);
		w_x(1, 2) = -p_w(0, 0);
		w_x(2, 1) =  p_w(0, 0);

		return w_x;
	}

	/**
	 * @brief Normalize a rotation vector.
	 *
	 * @param[in] p_w Rotation vector that should be normalized.
	 * @param[out] p_out_theta Norm of the rotation vector (Angle of rotation).
	 * @param[out] p_out_w_hat Normalized rotation vector (Axis of rotation).
	 */
	template <typename T>
	void normalizeRotationVector(const Eigen::Matrix<T, 3, 1>& p_w, T& p_out_theta, Eigen::Matrix<T, 3, 1>& p_out_w_hat)
	{
		p_out_theta = p_w.norm();
		p_out_w_hat = p_w.normalized();
	}

	/**
	 * @brief Convenient exponential map to pass from the cartesian vector space \f$\mathbb{R}^{3}\f$ to the associated Lie group \f$\text{SO}(3)\f$.
	 * More generically speaking, it converts a rotation vector to the associated rotation matrix.
	 *
	 * This formula is also known as the Rodrigues rotation formula.
	 *
	 * @param[in] p_w Rotation vector.
	 * @return Eigen::Matrix<T, 3, 3> Associated rotation matrix.
	 */
	template <typename T>
	Eigen::Matrix<T, 3, 3> Exp(const Eigen::Matrix<T, 3, 1>& p_w)
	{
		T theta = T(0);
		Eigen::Matrix<T, 3, 1> w_hat = Eigen::Matrix<T, 3, 1>::Zero();
		normalizeRotationVector<T>(p_w, theta, w_hat);

		Eigen::Matrix<T, 3, 3> w_hat_x = hat(w_hat);

		return Eigen::Matrix<T, 3, 3>::Identity() + sin(theta) * w_hat_x + (T(1) - cos(theta)) * (w_hat_x * w_hat_x);
	};

	/**
	 * @brief Convenient logarithm map to pass from the Lie group \f$\text{SO}(3)\f$ to the associated cartesian vector space \f$\mathbb{R}^{3}\f$.
	 * More generically speaking, it converts a rotation matrix to the associated rotation vector.
	 *
	 * @param[in] p_R Rotation matrix.
	 * @param[in] p_checkValidity Should we check that the input matrix is valid or not ?
	 * @return Eigen::Matrix<T, 3, 1> Associated rotation vector.
	 */
	template <typename T>
	Eigen::Matrix<T, 3, 1> Log(const Eigen::Matrix<T, 3, 3>& p_R, const bool p_checkValidity = true)
	{
		if (p_checkValidity && !isInSO3<T>(p_R))
		{
			throw std::runtime_error("Input matrix is not in SO(3).");
		}

		Eigen::AngleAxis<T> Raa(p_R);
		return Raa.angle() * Raa.axis();
	}
}