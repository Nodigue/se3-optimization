#pragma once

#include "TransformOperations.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ceres/ceres.h>

#include <stdexcept>

namespace SE3
{
    namespace optimization
    {
        template <typename T>
        Eigen::Matrix<T, 12, 1> toVector(const Eigen::Matrix<T, 4, 4>& p_matrix)
        {
            return p_matrix.template block<3, 4>(0, 0).reshaped(12, 1);
        }

        template <typename T>
        Eigen::Matrix<T, 4, 4> toMatrix(const Eigen::Matrix<T, 12, 1>& p_vector)
        {
            Eigen::Matrix<T, 4, 4> matrix = Eigen::Matrix<T, 4, 4>::Identity();
            matrix.template block<3, 4>(0, 0) = p_vector.reshaped(3, 4);

            return matrix;
        }

        // Using the full transformation matrix
        class CostFunctor
        {
            public:
                explicit CostFunctor(const Eigen::Matrix4d& p_Tm)
                {
                    m_Tm = toVector<double>(p_Tm);
                }

                template <typename T>
                bool operator()(const T* p_Tc, T* p_residuals) const
                {
                    const Eigen::Matrix<T, 4, 4> Tm = toMatrix<T>(m_Tm.cast<T>());

                    const Eigen::Map<const Eigen::Matrix<T, 12, 1>> Tc_map(p_Tc);
                    const Eigen::Matrix<T, 4, 4> Tc = toMatrix<T>(Tc_map);

                    Eigen::Map<Eigen::Matrix<T, 6, 1>> residuals(p_residuals);

                    residuals = SE3::rightMinus<T>(Tm, Tc);

                    return true;
                }

            private:
                Eigen::Matrix<double, 12, 1> m_Tm;
        };

        /**
         * @brief SE3 Manifold
         */
        class Manifold : public ceres::Manifold
        {
            /**
             * @brief Dimension of the ambient space in which the manifold is embedded.
             * The ambiant size
             */
            int AmbientSize() const final
            {
                return 12;
            }

            /**
             * @brief Dimension of the manifold / tangent space.
             */
            int TangentSize() const final
            {
                return 6;
            }

            /**
             * @brief Plus computes the result of moving along delta in the tangent space at x, and then projecting back
             * onto the manifold that x belongs to.
             */
            bool Plus(const double* p_x, const double* p_delta, double* p_x_plus_delta) const final
            {
                const Eigen::Map<const Eigen::Matrix<double, 12, 1>> x_map(p_x);
                const Eigen::Matrix4d x = toMatrix<double>(x_map);

                const Eigen::Map<const Eigen::Matrix<double, 6, 1>> delta(p_delta);

                Eigen::Map<Eigen::Matrix<double, 12, 1>> x_plus_delta_map(p_x_plus_delta);
                const Eigen::Matrix4d x_plus_delta = rightPlus<double>(x, delta);

                x_plus_delta_map = toVector<double>(x_plus_delta);

                return true;
            }

            /**
             * @brief Compute the derivative of Plus(x, delta) w.r.t delta at delta = 0
             */
            bool PlusJacobian(const double* p_x, double* p_jacobian) const final
            {
                const Eigen::Map<const Eigen::Matrix<double, 12, 1>> x_map(p_x);
                Eigen::Matrix4d x = toMatrix<double>(x_map);

                Eigen::Map<Eigen::Matrix<double, 12, 6, Eigen::RowMajor>> jacobian(p_jacobian);
                jacobian = Eigen::Matrix<double, 12, 6, Eigen::RowMajor>::Zero();

                jacobian.block<3, 1>(0, 4) = -x.block<3, 1>(0, 2);
                jacobian.block<3, 1>(0, 5) =  x.block<3, 1>(0, 1);
                jacobian.block<3, 1>(3, 3) =  x.block<3, 1>(0, 2);
                jacobian.block<3, 1>(3, 5) = -x.block<3, 1>(0, 0);
                jacobian.block<3, 1>(6, 3) = -x.block<3, 1>(0, 2);
                jacobian.block<3, 1>(6, 4) =  x.block<3, 1>(0, 0);

                jacobian.block<3, 3>(9, 0) = x.block<3, 3>(0, 0);

                return true;
            }

            /**
             * @brief Given two points x and y on the manifold, Minus computes the change to x in the tangent space at x,
             * that will take it to y.
             */
            bool Minus(const double* p_y, const double* p_x, double* p_y_minus_x) const final
            {
                const Eigen::Map<const Eigen::Matrix<double, 12, 1>> y_map(p_y);
                Eigen::Matrix4d y = toMatrix<double>(y_map);

                const Eigen::Map<const Eigen::Matrix<double, 12, 1>> x_map(p_x);
                Eigen::Matrix4d x = toMatrix<double>(x_map);

                Eigen::Map<Eigen::Matrix<double, 6, 1>> y_minus_x(p_y_minus_x);

                y_minus_x = rightMinus<double>(y, x);

                return true;
            }

            /**
             * @brief Compute the derivative of Minus(y, x) w.r.t y at y = 0.
             */
            bool MinusJacobian(const double* p_x, double* p_jacobian) const final
            {
                const Eigen::Map<const Eigen::Matrix<double, 12, 1>> x_map(p_x);
                Eigen::Matrix4d x = toMatrix<double>(x_map);

                Eigen::Map<Eigen::Matrix<double, 6, 12>> jacobian(p_jacobian);
                jacobian = Eigen::Matrix<double, 6, 12>::Zero();

                return true;
            }
        };

        Manifold* getManifold()
        {
            return new Manifold;
        }

        class Solver
        {
            public:
                explicit Solver(const Eigen::Matrix4d& p_T_init, const std::vector<Eigen::Matrix4d>& p_TmList):
                    m_TmList(p_TmList)
                {
                    m_T = toVector<double>(p_T_init);
                    m_problem.AddParameterBlock(m_T.data(), 12, getManifold());

                    for (const auto& Tm : m_TmList)
                    {
                        CostFunctor* c = new CostFunctor(Tm);
                        ceres::CostFunction* cost_function = new ceres::AutoDiffCostFunction<CostFunctor, 6, 12>(c);

                        m_problem.AddResidualBlock(cost_function, nullptr, m_T.data());
                    }
                }

                void solve()
                {
                    ceres::Solver::Options options;
                    options.linear_solver_type = ceres::DENSE_QR;
                    options.max_num_iterations = 100;
                    options.minimizer_progress_to_stdout = true;

                    ceres::Solver::Summary summary;
                    ceres::Solve(options, &m_problem, &summary);

                    std::cout << summary.FullReport() << "\n";
                }

                const Eigen::Matrix4d getEstimate() const
                {
                    return toMatrix<double>(m_T);
                }

            private:
                ceres::Problem m_problem;

                Eigen::Matrix<double, 12, 1> m_T;
                std::vector<Eigen::Matrix4d> m_TmList;
        };
    };
};